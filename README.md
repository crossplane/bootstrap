# bootstrap

Bootstrap a AWS crossplane cluster with FLUX linked to this GitLab Group

## Idea

- Create k3s cluster using multipass and install crossplane and flux to k3s
- Link flux to eks cluster repository and let crossplane create an eks cluster
- Install crossplane and flux on the new eks cluster
- Link flux in new eks cluster to eks cluster repository - should not change anything, because cluster exists
- Delete multipass k3s cluster

## Details

